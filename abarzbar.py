from numpy import *

def abar(x, a):
    abarInv = 0.
    for i in range(size(x)):
        abarInv += x[i]/a[i]
    return 1./abarInv

def zbar(x, a, z):
    abarInv = 0.
    zsum = 0.
    for i in range(size(x)):
        abarInv += x[i]/a[i]    
    for i in range(size(z)):
        zsum += z[i]*x[i]/a[i]
    return zsum/abarInv

def renorm(x):
    for i in range(size(x)):
        x[i] = fmax(x[i],1e-20)
    tot = x.sum()
    return x/tot

def fixye(awant, zwant, x0, isos, a, z,doprint):
    ye0 =  zbar(x0, a, z) / abar(x0, a)
    ye =  zbar(x0, a, z) / abar(x0, a)
    yewant = zwant / awant    
    x = zeros(size(x0))
    x[:] = x0[:]
    err = 1.0e-12
    if yewant < 0.5:
        # First, get rid of all ti44, cr48, and fe52
        x[where(isos=='fe54')] += x[where(isos=='ti44')]
        x[where(isos=='ti44')] = 0.0
        x[where(isos=='fe54')] += x[where(isos=='cr48')]
        x[where(isos=='cr48')] = 0.0
        x[where(isos=='fe54')] += x[where(isos=='fe52')]
        x[where(isos=='fe52')] = 0.0        
        ye =  zbar(x, a, z) / abar(x, a)
        if doprint: print 'ti44,cr48,fe52 -> fe54', ye0, ye, yewant
        if abs(ye-yewant) <= err: return x
        
    # The order of the flows will depend on starting Ye
    if ye > yewant:
        # fe54 -> fe56
        yeiter(yewant, x, where(isos=='fe56'), where(isos=='fe54'), a, z, err)
        ye =  zbar(x, a, z) / abar(x, a)
        if doprint: print 'fe54 -> fe56', ye0, ye, yewant
        if abs(ye-yewant) <= err: return x
        # fe54 -> fe56
        yeiter(yewant, x, where(isos=='fe56'), where(isos=='fe54'), a, z, err)
        ye =  zbar(x, a, z) / abar(x, a)
        if doprint: print 'fe54 -> fe56', ye0, ye, yewant
        if abs(ye-yewant) <= err: return x
        # fe56 -> cr56
        yeiter(yewant, x, where(isos=='cr56'), where(isos=='fe56'), a, z, err)
        ye =  zbar(x, a, z) / abar(x, a)
        if doprint: print 'fe56 -> cr56', ye0, ye, yewant
        if abs(ye-yewant) <= err: return x
        # cr48 -> cr56
        yeiter(yewant, x, where(isos=='cr56'), where(isos=='cr48'), a, z, err)
        ye =  zbar(x, a, z) / abar(x, a)
        if doprint: print 'cr48 -> cr56', ye0, ye, yewant
        if abs(ye-yewant) <= err: return x
        # ti44 -> cr56
        yeiter(yewant, x, where(isos=='cr56'), where(isos=='ti44'), a, z, err)
        ye =  zbar(x, a, z) / abar(x, a)
        if doprint: print 'ti44 -> cr56', ye0, ye, yewant
        if abs(ye-yewant) <= err: return x
        # ca40 -> cr56
        yeiter(yewant, x, where(isos=='cr56'), where(isos=='ca40'), a, z, err)
        ye =  zbar(x, a, z) / abar(x, a)
        if doprint: print 'ca40 -> cr56', ye0, ye, yewant
        if abs(ye-yewant) <= err: return x
        # ar36 -> cr56
        yeiter(yewant, x, where(isos=='cr56'), where(isos=='ar36'), a, z, err)
        ye =  zbar(x, a, z) / abar(x, a)
        if doprint: print 'ar36 -> cr56', ye0, ye, yewant
        if abs(ye-yewant) <= err: return x
        # s32 -> cr56
        yeiter(yewant, x, where(isos=='cr56'), where(isos=='s32'), a, z, err)
        ye =  zbar(x, a, z) / abar(x, a)
        if doprint: print 's32 -> cr56', ye0, ye, yewant
        if abs(ye-yewant) <= err: return x
        # si28 -> cr56
        yeiter(yewant, x, where(isos=='cr56'), where(isos=='si28'), a, z, err)
        ye =  zbar(x, a, z) / abar(x, a)
        if doprint: print 'si28 -> cr56', ye0, ye, yewant
        if abs(ye-yewant) <= err: return x
    elif ye < yewant:
        yeiter(yewant, x, where(isos=='cr56'), where(isos=='fe56'), a, z, err)
        ye =  zbar(x, a, z) / abar(x, a)
        if doprint: print 'cr56 -> fe56', ye0, ye, yewant
        if abs(ye-yewant) <= err: return x
        yeiter(yewant, x, where(isos=='fe56'), where(isos=='fe54'), a, z, err)
        ye =  zbar(x, a, z) / abar(x, a)
        if doprint: print 'fe56 -> fe54', ye0, ye, yewant
        if abs(ye-yewant) <= err: return x
        yeiter(yewant, x, where(isos=='fe54'), where(isos=='fe52'), a, z, err)
        ye =  zbar(x, a, z) / abar(x, a)
        if doprint: print 'fe54 -> fe52', ye0, ye, yewant
        if abs(ye-yewant) <= err: return x
    return x


def yeiter(yewant, x, iso1, iso2, a, z, err):
    # ye_iso1 < ye_iso2
    # for ye > yewant, iso2 => iso1
    # for ye < yewant, iso1 => iso2
    run = yeroot(yewant, x, iso1, iso2, a, z)
    ye =  zbar(x, a, z) / abar(x, a)
    ydir = sign(yewant - ye)
    if ydir < 0. and x[iso2] > 0.:
        dx = min(0.1, x[iso2])
    elif ydir > 0. and x[iso1] > 0.:
        dx = -min(0.1, x[iso1])
    #print run, ye, ydir, x[iso1], x[iso2]
    while run:
        x[iso1] += dx
        x[iso2] -= dx
        ye = zbar(x, a, z) / abar(x, a)
        #print ye, x[iso1], x[iso2]
        if abs(ye - yewant) <= err:
            run = False
        elif sign(yewant - ye) != ydir:
            # we have passed the root.  bisection!
            dx *= -0.5
            ydir *= -1.0

def yeroot(yewant, x, iso1, iso2, a, z):
    ye =  zbar(x, a, z) / abar(x, a)
    if ye > yewant:
        x[iso1] += x[iso2]
        x[iso2] = 0.
        ye =  zbar(x, a, z) / abar(x, a)
        if ye < yewant:
            return True
    elif ye < yewant:
        x[iso2] += x[iso1]
        x[iso1] = 0.
        ye =  zbar(x, a, z) / abar(x, a)
        if ye > yewant:
            return True
    return False

def x204to21(data,iso,a,z,iso21):
    iso[where(iso == 'd')] = 'h2'
    iso[where(iso == 'prot')] = 'h1'

    x204 = zeros((size(iso), size(data['neut'])),dtype=float64)
    for i in iso:
        if i != 'o14':
            x204[asarray(where(iso==i))[0][0],:] += data[i]

    x = zeros((size(iso21),size(data['neut'])),dtype=float64)
    x[0,:] = (data['h1'])+(data['h2'])
    x[1,:] = data['he3']
    # he4
    ind = where((a>3)&(a<=6))
    for i in iso[ind]:
        x[2,:] += data[i]
    # c12
    ind = where((a>6) & (a<=13))
    for i in iso[ind]:
        x[3,:] += data[i]
    # n14
    ind = where((a>13) & (a<=15))
    for i in iso[ind]:
        if i != 'o14':
            x[4,:] += data[i]
    # o16
    ind = where((a>15) & (a<=18))
    for i in iso[ind]:
        x[5,:] += data[i]
    # ne20
    ind = where((a>18) & (a<=22))
    for i in iso[ind]:
        x[6,:] += data[i]
    # mg24
    n = 7 
    ind = where((a>22) & (a<=28))
    for i in iso[ind]:
        if i != 'si28':
            x[n,:] += data[i]
    # si28
    n = 8
    ind = where(a==28)
    for i in iso[ind]:
        if i == 'si28':
            x[n,:] += data[i]
    # s32
    n = 9
    ind = where((a>28)&(a<36))
    for i in iso[ind]:
        x[n,:] += data[i]
    # ar36
    n = 10
    ind = where((a>35)&(a<40))
    for i in iso[ind]:
        x[n,:] += data[i]
    # ca40
    n = 11
    ind = where((a>39)&(a<44))
    for i in iso[ind]:
        x[n,:] += data[i]
    # ti44
    n = 12
    ind = where((a>43)&(a<48))
    for i in iso[ind]:
        x[n,:] += data[i]
    # cr48
    n = 13
    ind = where((a>47)&(a<52))
    for i in iso[ind]:
        x[n,:] += data[i]
    # cr56
    n = 14
    ind = where((a>56))
    for i in iso[ind]:
        x[n,:] += data[i]
    # fe52
    n = 15
    ind = where((a>51)&(a<53))
    for i in iso[ind]:
        x[n,:] += data[i]
    # fe54
    n = 16
    ind = where((a>52)&(a<55))
    for i in iso[ind]:
        x[n,:] += data[i]
    # fe56
    n = 17
    ind = where((a>54)&(a<57))
    for i in iso[ind]:
        if i != 'ni56':
            x[n,:] += data[i]
    # ni56
    n = 18
    ind = where(a==56)
    for i in iso[ind]:
        if i == 'ni56':
            x[n,:] += data[i]
    # neutrons
    n = 19
    ind = where(iso=='neut')
    for i in iso[ind]:
        x[n,:] += data[i]

    return x204, x


def write_model(data, x, ye, isos, name, header):
    # Assume that reordering of data is necessary
    # could be a check for this, but... lazy.
    isos[where(isos=='n')] = 'neut'
    data = data[::-1]
    x = x[:,::-1]
    ye = ye[::-1]

    # Now pull out some key quantities
    radius = data['radius']*6.955e10 # cm
    dens = 10.0**data['logRho']
    temp = data['temperature']
    pres = data['pressure']
    velx = data['velocity']
    entr = data['entropy']

    # Fix the velocity.  Vel in MESA is face value.
    vel0 = zeros(size(velx))
    vel0[0] = 0.5*velx[0] # assume v(r=0) = 0.
    for i in range(1,size(velx)):
        vel0[i] = 0.5*(velx[i] + velx[i-1])
    velx = vel0
    
    # Fix radius
    rad = zeros(size(radius))
    rad[0] = 0.5*radius[0]
    for i in range(1,size(radius)):
        rad[i] = 0.5*(radius[i] + radius[i-1])
    radius = rad
    
    f = open(name, 'w')

    f.write('# '+header+'\n')
    f.write('number of variables = 26\n')
    f.write('dens\n')
    f.write('temp\n')
    f.write('ye\n')
    f.write('velx\n')
    f.write('pres\n')
    f.write('entr\n')
    for i in isos:
        f.write(i+"\n")
    for i in range(size(radius)):
        f.write(str(radius[i])+" ")
        f.write(str(dens[i])+" ")
        f.write(str(temp[i])+" ")
        f.write(str(ye[i])+" ")
        f.write(str(velx[i])+" ")
        f.write(str(pres[i])+" ")
        f.write(str(entr[i])+" ")
        for j in range(size(x[:,i])):
            f.write(str(x[j,i])+" ")
        f.write('\n')
    f.close()
